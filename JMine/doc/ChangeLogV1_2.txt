JMine V1.2 Change Log
1.修正了线程类不建议使用的方法
2.不再使用Thread和Runnable二是改用java.util.Timer
3.界面改善
4.修正了一些Exception
5.现在您可以在文件夹下直接javac *.java不会再有任何提示或错误
6.本例中class文件在jdk1.6下以javac -target 1.5 *.java生成在jdk1.5和jdk1.6中都能运行
7.有任何疑问和建议,可用email:jerry.shen@cognizant.com或jerry_shen_sjf@yahoo.com.cn和作者联系